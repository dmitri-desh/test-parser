﻿using DAL.Interfaces;
using NLog;
using System;

namespace LoggerLib
{
	public class LoggerHelper : ILoggerHelper
	{
		public static readonly ILogger Current;

		public void Error(string message)
		{
			if (Current != null)
			{
				Current.Error($"ERROR: {message}");
			}
			Console.WriteLine($@"Error: {message}");
		}

		public void Message(string message)
		{
			if (Current != null)
			{
				Current.Info($"------- {message}");
			}
			Console.WriteLine($@"Message: {message}");
		}

		public void Warning(string message)
		{
			if (Current != null)
			{
				Current.Warn($"WARNING: {message}");
			}
			Console.WriteLine($@"Warning: {message}");
		}
	}
}
