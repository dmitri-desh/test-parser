﻿using DAL.Models;
using SecurityLib;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace DAL
{
	public class DbInitializer : DropCreateDatabaseIfModelChanges<NewsContext>
	{
		protected override void Seed(NewsContext context)
		{
			IList<Role> defaultRoles = new List<Role>();
			defaultRoles.Add(new Role() { RoleId = 1, RoleName = "Admin"});
			defaultRoles.Add(new Role() { RoleId = 2, RoleName = "User" });
			context.Roles.AddRange(defaultRoles);

			User defaultUser = new User() { UserId = 1, UserName = "Dimon", Password = Encryption.Encrypt("dimpas01"), CreatedOn = DateTime.Now, Email = "email@email.com", RoleId = 1};
			context.Users.Add(defaultUser);

			base.Seed(context);
		}
	}
}
