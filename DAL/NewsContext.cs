﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class NewsContext : DbContext
    {
		public DbSet<Site> Sites { get; set; }
		public DbSet<NewsPost> NewsPosts { get; set; }
		public DbSet<Comment> Comments { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<Token> Tokens { get; set; }

		public NewsContext() : base()
		{
			//Database.SetInitializer(new );
		}
    }
}
