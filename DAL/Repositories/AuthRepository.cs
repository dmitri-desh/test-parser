﻿using DAL.Interfaces;
using DAL.Models;
using SecurityLib;
using System;
using System.Linq;

namespace DAL.Repositories
{
	public class AuthRepository : IAuthRepository
	{
		private readonly NewsContext _context;

		public int DeleteToken(string userName)
		{
			try
			{
				var token = _context.Tokens.SingleOrDefault(t => t.User.UserName == userName);
				_context.Tokens.Remove(token);
				return _context.SaveChanges();
			}
			catch (Exception)
			{
				throw;
			}
		}

		public string GetRoleBy(string userName)
		{
			try
			{
				var roleName = _context.Users.Include("Role").FirstOrDefault(r => r.UserName == userName).Role.RoleName;
				return roleName;
			}
			catch (Exception)
			{
				throw;
			};
		}

		public Token GetToken(string userName)
		{
			try
			{
				var token = _context.Tokens.FirstOrDefault(t => t.User.UserName == userName);
				return token;
			}
			catch
			{
				throw;
			}
		}

		public User GetUser(string userName, string userPassword)
		{
			try
			{
				var pass = Encryption.Encrypt(userPassword);
				var result = _context.Users.Include("Role").FirstOrDefault(r => r.UserName == userName && r.Password == pass);
				return result;
			}
			catch (Exception)
			{
				throw;
			}
		}

		public User GetUserBy(string roleName, string userName)
		{
			try
			{
				var result = _context.Users.Include("Role").FirstOrDefault(r => r.UserName == userName && r.Role.RoleName == roleName);
				return result;
			}
			catch (Exception)
			{
				throw;
			}
		}

		public int InsertToken(Token token)
		{
			try
			{
				_context.Tokens.Add(token);
				return _context.SaveChanges();
			}
			catch (Exception)
			{
				throw;
			}
		}

		public bool IsTokenExists(string usreName)
		{
			try
			{
				var result = _context.Tokens.FirstOrDefault(t => t.User.UserName == usreName);
				return result != null ? true : false;
			}
			catch (Exception)
			{
				throw;
			}
		}
	}
}
