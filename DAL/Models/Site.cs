﻿namespace DAL.Models
{
	public class Site
	{
		public int SiteId { get; set; }
		public string SiteName { get; set; }
		public string SiteUrl { get; set; }
	}
}
