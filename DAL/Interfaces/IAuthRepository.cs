﻿using DAL.Models;

namespace DAL.Interfaces
{
	public interface IAuthRepository
	{
		User GetUser(string userName, string userPassword);
		User GetUserBy(string roleName, string userName);
		string GetRoleBy(string userName);
		Token GetToken(string userName);
		bool IsTokenExists(string usreName);
		int InsertToken(Token token);
		int DeleteToken(string userName);
	}
}
