﻿using DAL.Models;
using System.Collections.Generic;

namespace DAL.Interfaces
{
	public interface ICommentRepository : IRepository<Comment>
	{
		IEnumerable<Comment> GetAnyBy(int id);
		void Delete(int id);
	}
}
