﻿using DAL.Models;

namespace DAL.Interfaces
{
	public interface IRegisterUserRepository
	{
		void Add(User user);
		bool IsUserExists(string userName);
		int GetUserId(string userName);
	}
}
